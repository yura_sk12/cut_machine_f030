#pragma once

#include "stm32f0xx.h"
#include "stdint.h"
#include "LCD1604.h"
#include "Servo.h"
#include "Encoder.h"
#include "sensor.h"
#include "flash_mem.h"
//#include "globals.h"
//#include "sensor.h"

// �������� ������������ �������
// ������������ �����
// ������������ SYSCFG

// ������������� ��� ����� ���������� ������� ������ �������� �������
// ��� ������� ������ � ����
#define CHANGE_MENU_DELAY 2000

// ����� ������� ���������� ������� ������ start ��� ������
#define RESET_CUT_CNT_DELAY 4000

// ����� �� 50 �� ������ ��� �� ���� ���� �� �� ���� �������������� �� 50 ������ ��������
#define ENCODER_CNT_IMPULS 10000/1

// ������ ���������� ������� ����� ����
// ������ ������������ �� �������� � �������
//#define ROTATE_LENGTH 344  // mm
// ������������ ������ ����� � ��������� � ��
#define ERROR_CORRECT 0

#define START_BTN_PORT GPIOA
#define START_BTN_PIN 1
// ��� ��������� �������������� ��� ����������
#define START_BTN_SYSCFG SYSCFG_EXTICR1_EXTI1_PA
// ����� ���������� ��������
#define START_BTN_IRQ EXTI0_1_IRQn
// ������� ����������
#define START_BTN_IRQHandler EXTI0_1_IRQHandler()
#define START_BTN_PRIORITY 2
#define SYSTICK_PRIORITY 0

// ���� ��� ���������� �����
#define CUT_PORT GPIOA
// ��� ��� ���������� �����
#define CUT_PIN 0
// �������� ���
#define CUT_DOWN CUT_PORT->ODR |= 1 << CUT_PIN
// ������� ���
#define CUT_UP CUT_PORT->ODR &= ~(1 << CUT_PIN)
