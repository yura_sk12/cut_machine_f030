#pragma once

#include "stm32f0xx.h"
#include "stdlib.h"
#include "stdint.h"

#define F_CPU 8000000

/*
 *
 * 	������ �������������
 * 	RCC->AHBENR |= RCC_AHBENR_GPIODEN;
	LCD_init();
	�������:
	��������� ���� ������������ ����
	�������� ������������
	��������� LCD_init();
*/

// ���� ����� ������� �� ���������������
//#define USE_EN

#define PIN_RS		7
#define PIN_RW		6
#define PIN_E		5  //
#define PIN_DB4		4
#define PIN_DB5		3
#define PIN_DB6		1
#define PIN_DB7		0

#define PORT_RS		GPIOB
#define PORT_RW		GPIOB
#define PORT_E		GPIOB
#define PORT_DB4	GPIOB
#define PORT_DB5	GPIOB
#define PORT_DB6	GPIOB
#define PORT_DB7	GPIOB

void __init_ports();
void __wait_us(uint32_t us);
void __wait_ms(uint32_t ms);

// ����� ������� � ������� �������
void __send_databyte(uint8_t bt);
void __send_half_byte(uint8_t bt);
void __send_full_byte(uint8_t bt);
uint8_t __strlen(char * str);

void LCD_init();
void LCD_command(uint8_t bt);
void LCD_time_test();
void LCD_send_string_eng(char *str);
void LCD_send_string_rus(uint8_t *str);
void LCD_send_string(uint8_t *str);  // ��� �������� ������ �� ���� ������
void LCD_set_position(uint8_t x, uint8_t y);
void LCD_return_home();
void LCD_send_int(int number, uint8_t posx, uint8_t posy);
void LCD_cursor_on();
void LCD_cursor_off();
void LCD_cursor_blink_on();
void LCD_cursor_blink_off();
void LCD_cursor_dir(uint8_t right);
void LCD_display_on();
void LCD_display_off();
void LCD_clear();
