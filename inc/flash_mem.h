#pragma once

#include "stm32f0xx.h"
#include "stdint.h"

#define PAGE_FOR_WRITE 31						// ����� �������� �� ���� ��� ������ ����������
#define ADDR_FOR_WRITE PAGE_FOR_WRITE * 0x400	// ����� ������ PAGE_FOR_WRITE ��������

void __flash_unclock_mem();
void __flash_lock_mem();

// ������� ��������
void __flash_internal_erase();

//data - ��������� �� ������������ ������
//address - ����� �� flash
//count - ���������� ������������ ����, ������ ���� ������ 2
void __flash_internal_write(unsigned char* data, unsigned int address, unsigned int count) ;
uint32_t __flash_read_from_addr(uint32_t addr);
void flash_write_all_param();
void flash_read_all_param();
