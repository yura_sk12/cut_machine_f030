#pragma once

#include "stm32f0xx.h"
#include "stdint.h"

void sensor_init();
uint8_t sensor_state();

#define SENSOR_PORT GPIOA
#define SENSOR_PIN 11
