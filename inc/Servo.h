#pragma once

#include "stm32f0xx.h"
#include "stdint.h"
#include "sensor.h"

#define F_TIM 8000000

/*
	�������:
	�������� ������������ GPIO � �������
	��������� servo_init();
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
 * */

#define SERVO_TIMER TIM3
#define SERVO_TIMIRQ TIM3_IRQn
#define SERVO_TIMHandler TIM3_IRQHandler
#define SERVO_PORT1	GPIOA  // ���� �������� ����������� �������
#define SERVO_PORT2	GPIOA  // ���� �������� ����������� ��������
#define SERVO_PIN1	6
#define SERVO_PIN2	7
#define SERVO_PRIORITY 4

void __servo_en_timer();
void __servo_dis_timer();

// ������������� ������ � �������
void servo_init();

// �������� ������ �� ������ � ��
void servo_rotate_forward(uint16_t cnt);

// �������� ������ �� ������ � ��
void servo_rotate_backward(uint16_t cnt);

void servo_wait();

// ������� ���������� ������� �������
void SERVO_set_speed(uint16_t speed);
