#pragma once

#include "stm32f0xx.h"
#include "stdint.h"
#include "LCD1604.h"
#include "Servo.h"

// �������� ������������ ���� ������
// RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
// RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
// RCC->AHBENR |= RCC_AHBENR_GPIOEEN;
// RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;


#define ENCODER_PORT GPIOA
#define ENCODER_BUTTON_PORT	GPIOA
#define ENCODER_PINA 8
#define ENCODER_PINB 9
#define ENCODER_BUTTON_PIN 12
#define ENCODER_PRIORITY 3

#define ENCODER_SYSCFG SYSCFG_EXTICR4_EXTI12_PA
#define ENCODER_PINA_SYSCFG SYSCFG_EXTICR3_EXTI9_PA

#define ENCODER_IRQ EXTI4_15_IRQn
//#define ENCODER_PINA_IRQ EXTI9_5_IRQn

#define ENCODER_IRQHandler EXTI4_15_IRQHandler
//#define ENCODER_PINA_IRQHandler EXTI9_5_IRQHandler(void)

// �������� ������ �������� ����� ������
void Encoder_init();
// �������� ������ �������� ����� ����������
void Encoder_init_interrupt();
// ���������� ������� � ����� ��� �������� �������
void set_cursor_pos();
// 1 - ������� �������� ��� ������� ������, 0 - ������� �� ��������
uint8_t Encoder_isrotated();

void set_cursor_y(uint8_t pos);
void set_cursor_x(uint8_t pos);
void change_menu();
