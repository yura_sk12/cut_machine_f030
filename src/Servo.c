#include "Servo.h"

// ��� �������� ������������ �� ������ ���������� �������
uint64_t cnt_timer = 0;

// ������� ����� ������� ������ ��������
// ������ ������ ���������� ��� ��������
uint16_t servo_rotate_steps = 1000;

// �������� �������� ������������ - �������� � ��
uint16_t servo_rotate_speed_hz = 1000;

// ���� ��������� ��� ����������� ���������
// ����������� ��� ������� ������ ��������, ���� ���� �� 0
uint8_t servo_is_working = 0;

void servo_init(){
	//��������� ����� ��� ���������� ��������� ������������******
	// push-pull
	SERVO_PORT1->OTYPER &= ~(1 << SERVO_PIN1);
	// Output
	SERVO_PORT1->MODER |= (1 << (SERVO_PIN1 << 1));
	SERVO_PORT1->PUPDR |= (0b10 << (SERVO_PIN1 << 1));
	SERVO_PORT1->PUPDR &= ~(0b1 << (SERVO_PIN1 << 1));
	SERVO_PORT1->OSPEEDR |= 11 << (SERVO_PIN1 << 1);  // max speed
	//***********************************************************


	//��������� ����� ��� ���������� ����������� �������� ������������*****
	// push-pull
	SERVO_PORT2->OTYPER &= ~(1 << SERVO_PIN2);
	// Output
	SERVO_PORT2->MODER |= (1 << (SERVO_PIN2 << 1));
	SERVO_PORT2->PUPDR |= (0b10 << (SERVO_PIN2 << 1));
	SERVO_PORT2->PUPDR &= ~(0b1 << (SERVO_PIN2 << 1));
	SERVO_PORT2->OSPEEDR |= 11 << (SERVO_PIN2 << 1);  // max speed
	//*********************************************************************

	// ��������� �������� ������ ������� - �������� ������ �� �������� �������� ������������
	SERVO_TIMER->PSC = 8-1;
	SERVO_TIMER->ARR = F_TIM/servo_rotate_speed_hz/8 - 1;
	SERVO_TIMER->DIER |= TIM_DIER_UIE;
	//��������� ���������� �� TIM3
	NVIC_EnableIRQ(SERVO_TIMIRQ);
	NVIC_SetPriority(SERVO_TIMIRQ, SERVO_PRIORITY);
}

void SERVO_set_speed(uint16_t speed){
	SERVO_TIMER->ARR = (int)((float)F_TIM/(float)speed/(float)8 - 1);
}

void SERVO_TIMHandler(){
	if (cnt_timer >= servo_rotate_steps*2){
		servo_is_working = 0;
		cnt_timer = 0;
		SERVO_PORT1->ODR &= ~(1 << SERVO_PIN1);
		__servo_dis_timer();
		SERVO_TIMER->SR &= ~TIM_SR_UIF;//�������� ���� ����������
		return;
	}
	cnt_timer += 1;
	if (sensor_state())  // ���� ������ ������ ����� �������������
		SERVO_PORT1->ODR ^= 1 << SERVO_PIN1;
	SERVO_TIMER->SR &= ~TIM_SR_UIF;//�������� ���� ����������
}

void servo_rotate_forward(uint16_t cnt){
	if (!servo_is_working){
		servo_rotate_steps = cnt;
//		servo_wait();
		SERVO_PORT2->ODR |= 1 << SERVO_PIN2;
		servo_is_working = 1;
		__servo_en_timer();
	}
}

void servo_rotate_backward(uint16_t cnt){
	if (!servo_is_working){
		servo_rotate_steps = cnt;
//		servo_wait();
		SERVO_PORT2->ODR &= ~(1 << SERVO_PIN2);
		servo_is_working = 1;
		__servo_en_timer();
	}
}

void servo_wait(){
	while(servo_is_working){} // ���� ���� ����� ��������
}

void __servo_en_timer(){
	SERVO_TIMER->CR1 |= TIM_CR1_CEN;
}

void __servo_dis_timer(){
	SERVO_TIMER->CR1 &= ~TIM_CR1_CEN;
}
