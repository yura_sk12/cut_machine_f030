#include "LCD1604.h"
// ����� ������������� �������� ������������ ������

volatile uint32_t timestamp_lcd = 0;

__inline__ static void __set_rs();
__inline__ static void __set_rw();
__inline__ static void __set_e();
__inline__ static void __set_data(uint8_t dt);

__inline__ static void __reset_rs();
__inline__ static void __reset_rw();
__inline__ static void __reset_e();

__inline__ static void __strob();


const char rus_arr_small[] = "��������������������������������";
const char rus_arr_big[] = "��������������������������������";

const char rus_arr_small_lcd_hex[] = {0x61,  // �
								0xB2,  // �
								0xB3,  // �
								0xB4,  // �
								0xE3,  // �
								0x65,  // �
								0xB6,  // �
								0xB7,  // �
								0xB8,  // �
								0xB9,  // �
								0xBA,  // �
								0xBB,  // �
								0xBC,  // �
								0xBD,  // �
								0x6F,  // �
								0xBE,  // �
								0x70,  // �
								0x63,  // �
								0xBF,  // �
								0x79,  // �
								0xE4,  // �
								0x78,  // �
								0xE5,  // �
								0xC0,  // �
								0xC1,  // �
								0xE6,  // �
								0xC2,  // �
								0xC3,  // �
								0xC4,  // �
								0xC5,  // �
								0xC6,  // �
								0xC7}; // �

const char rus_arr_big_lcd_hex[] =   {0x41,  // �
								0xA0,  // �
								0x42,  // �
								0xA1,  // �
								0xE0,  // �
								0x45,  // �
								0xA3,  // �
								0xA4,  // �
								0xA5,  // �
								0xA6,  // �
								0x4B,  // �
								0xA7,  // �
								0x4D,  // �
								0x48,  // �
								0x4F,  // �
								0xA8,  // �
								0x50,  // �
								0x43,  // �
								0x54,  // �
								0xA9,  // �
								0xAA,  // �
								0x58,  // �
								0xE1,  // �
								0xAB,  // �
								0xAC,  // �
								0xE2,  // �
								0xC2,  // �
								0xAE,  // �
								0x62,  // �
								0xAF,  // �
								0xB0,  // �
								0xB1}; // �

void __init_ports(){
	// ����� ���� - (01) General purpose output mode
	PORT_RS->MODER 	|= 1 << (PIN_RS << 1);
	PORT_RW->MODER 	|= 1 << (PIN_RW << 1);
	PORT_E->MODER 	|= 1 << (PIN_E << 1);
	PORT_DB4->MODER |= 1 << (PIN_DB4 << 1);
	PORT_DB5->MODER |= 1 << (PIN_DB5 << 1);
	PORT_DB6->MODER |= 1 << (PIN_DB6 << 1);
	PORT_DB7->MODER |= 1 << (PIN_DB7 << 1);
	//#######################################

	// ��� ������ - (0) Output push-pull
	PORT_RS->OTYPER  &= ~(1 << PIN_RS);
	PORT_RW->OTYPER  &= ~(1 << PIN_RW);
	PORT_E->OTYPER   &= ~(1 << PIN_E);
	PORT_DB4->OTYPER &= ~(1 << PIN_DB4);
	PORT_DB5->OTYPER &= ~(1 << PIN_DB5);
	PORT_DB6->OTYPER &= ~(1 << PIN_DB6);
	PORT_DB7->OTYPER &= ~(1 << PIN_DB7);
	//##############################

	// �������� - (10) Pull-down
	PORT_RS->PUPDR 	&= ~(1 << (PIN_RS << 1));
	PORT_RW->PUPDR 	&= ~(1 << (PIN_RW << 1));
	PORT_E->PUPDR 	&= ~(1 << (PIN_E << 1));
	PORT_DB4->PUPDR &= ~(1 << (PIN_DB4 << 1));
	PORT_DB5->PUPDR &= ~(1 << (PIN_DB5 << 1));
	PORT_DB6->PUPDR &= ~(1 << (PIN_DB6 << 1));
	PORT_DB7->PUPDR &= ~(1 << (PIN_DB7 << 1));

	PORT_RS->PUPDR 	|= 2 << (PIN_RS << 1);
	PORT_RW->PUPDR 	|= 2 << (PIN_RW << 1);
	PORT_E->PUPDR 	|= 2 << (PIN_E << 1);
	PORT_DB4->PUPDR |= 2 << (PIN_DB4 << 1);
	PORT_DB5->PUPDR |= 2 << (PIN_DB5 << 1);
	PORT_DB6->PUPDR |= 2 << (PIN_DB6 << 1);
	PORT_DB7->PUPDR |= 2 << (PIN_DB7 << 1);
	//#####################
}


__inline__ static void __set_rs(){
	PORT_RS->BSRR |= (1 << PIN_RS);
}

__inline__ static void __set_rw(){
	PORT_RW->BSRR |= (1 << PIN_RW);
}

__inline__ static void __set_e(){
	PORT_E->BSRR |= (1 << PIN_E);
}

__inline__ static void __set_data(uint8_t dt){
	// DB4 = 0b111(1)
	// DB5 = 0b11(1)1
	// DB6 = 0b1(1)11
	// DB7 = 0b(1)111

	if ((dt >> 0) & 0b1){
		PORT_DB4->BSRR |= (1 << PIN_DB4);
	}
	if ((dt >> 1) & 0b1){
		PORT_DB5->BSRR |= (1 << PIN_DB5);
	}
	if ((dt >> 2) & 0b1){
		PORT_DB6->BSRR |= (1 << PIN_DB6);
	}
	if ((dt >> 3) & 0b1){
		PORT_DB7->BSRR |= (1 << PIN_DB7);
	}

	dt = ~dt;
	if ((dt >> 0) & 0b1){
		PORT_DB4->ODR &= ~(1 << PIN_DB4);
	}
	if ((dt >> 1) & 0b1){
		PORT_DB5->ODR &= ~(1 << PIN_DB5);
	}
	if ((dt >> 2) & 0b1){
		PORT_DB6->ODR &= ~(1 << PIN_DB6);
	}
	if ((dt >> 3) & 0b1){
		PORT_DB7->ODR &= ~(1 << PIN_DB7);
	}
}

__inline__ static void __reset_rs(){
	PORT_RS->BSRR |= (1 << (PIN_RS + 16));
}

__inline__ static void __reset_rw(){
	PORT_RS->BSRR |= (1 << (PIN_RW + 16));
}

__inline__ static void __reset_e(){
	PORT_RS->BSRR |= (1 << (PIN_E + 16));
}

__inline__ static void __strob(){
	__set_e();
	__wait_us(1);
	__reset_e();
}

void __send_half_byte(uint8_t bt){
	__set_data(bt);
	__strob();
}

void __send_full_byte(uint8_t bt){
	__send_half_byte(bt >> 4);
	__send_half_byte(bt&0b00001111);
}

void __send_databyte(uint8_t bt){
	__set_rs();
	__reset_rw();
	__send_full_byte(bt);
	__wait_us(200);
}

void LCD_time_test(){
	__strob();
	__wait_ms(1);
}

void __wait_us(uint32_t us){
	for (uint32_t var = 0; var < us*2; ++var) {
//		asm("nop");
	}
}

void __wait_ms(uint32_t ms){
	for (uint32_t var = 0; var < ms; ++var) {
		for (int var2 = 0; var2 < 4000; ++var2) {
//			asm("nop");
		}
	}
}

void LCD_init(){
	__init_ports();
	__wait_ms(40);
	__reset_rs();
	__send_half_byte(3);
	__wait_ms(1);
	__send_half_byte(3);
	__wait_ms(1);
	__send_half_byte(3);
	__wait_ms(1);

	LCD_command(0x28);
	LCD_command(0x28);
	LCD_command(0x0F);
	LCD_command(0x01);
	LCD_command(0x06);
	LCD_command(0x02);
	LCD_cursor_off();
}

void LCD_command(uint8_t bt){
	__wait_us(300);
	__reset_rs();
	__reset_rw();
	__send_full_byte(bt);

	if (bt == 0x01)
		__wait_ms(4);
	else if (bt == 0x02 || bt == 0x03)
		__wait_ms(4);
	else
		__wait_us(300);
}

void LCD_send_string_eng(char *str){
	while(*str != '\0'){
		__send_databyte(*str);
		str++;
	}
}

void LCD_send_string_rus(uint8_t *str){
	while(*str != '\0'){
		if ((*str) < 224)
			__send_databyte(rus_arr_big_lcd_hex[(uint8_t)(*str - 192)]);
		else
			__send_databyte(rus_arr_small_lcd_hex[(uint8_t)(*str - 224)]);
		str++;
	}
}

void LCD_send_string(uint8_t *str){
	while(*str != '\0'){
		if ((*str) < 224 && (*str) > 191)
			__send_databyte(rus_arr_big_lcd_hex[(uint8_t)(*str - 192)]);
		else if ((*str) > 223 && (*str) < 224+32)
			__send_databyte(rus_arr_small_lcd_hex[(uint8_t)(*str - 224)]);
		else
			__send_databyte(*str);
		str++;
	}
}

void LCD_set_position(uint8_t x, uint8_t y){
	uint8_t pos_code = x%16; // �� ������ 16 ���� ��������� ������
	if (y%4 == 0)
		pos_code |= 0x00;	 // ������ ������
	if (y%4 == 1)
		pos_code |= 0x40;	 // ������ ������
	if (y%4 == 2)
		pos_code |= 0x10;	 // ������ ������
	if (y%4 == 3)
		pos_code |= 0x50;	 // ��������� ������
	pos_code |= 0b10000000;  	 // ��� ��������� ������� SDRAM RS = 0, RW = 0 DB1 = 1
	LCD_command(pos_code);
}

void LCD_return_home(){
	LCD_command(0b10);
}

uint8_t __strlen(char * str){
	uint8_t _strlen_ = 0;
	while(*str != '\0'){
		_strlen_ += 1;
		str++;
	}
	return _strlen_;
}

void LCD_send_int(int number, uint8_t posx, uint8_t posy){
	char arr[5];
	itoa(number, (char *)arr, 10);

	uint8_t str_len = 0;  // ���-�� �������� � �����
	str_len = __strlen((char *)arr);  // ��� ������� ����� ��� ���� �����
	LCD_set_position(posx - str_len+1, posy);
//	uint8_t str_len = 0;  // ���-�� �������� � �����
//	str_len = __strlen((char *)arr);  // ��� ������� ����� ��� ���� �����
//	for (int var = str_len; var < len; ++var) {
//		arr[var] = '0';
//	}
//	arr[len] = '\0';

	LCD_send_string_eng(arr);
}

void LCD_cursor_on(){
	LCD_command(0b00001111);
}

void LCD_cursor_off(){
	LCD_command(0b00001100);
}

void LCD_cursor_blink_on(){
	LCD_command(0b00001111);
}

void LCD_cursor_blink_off(){
	LCD_command(0b00001110);
}

void LCD_display_on(){
	LCD_command(0b00001111);
}

void LCD_display_off(){
	LCD_command(0b00001000);
}

void LCD_clear(){
	LCD_command(0b1);
}

void LCD_cursor_dir(uint8_t right){
	if (right){
		LCD_command(0b10100);
	} else {
		LCD_command(0b10000);
	}
}
