#include "sensor.h"

void sensor_init(){
	SENSOR_PORT->MODER &= ~(0b11 << (SENSOR_PIN << 1));
}

uint8_t sensor_state(){
	if ((SENSOR_PORT->IDR & (1 << SENSOR_PIN)))
		return 1;
	else
		return 0;
}
