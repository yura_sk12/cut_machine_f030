#include "flash_mem.h"

/* ��������� ��� ������ �� ���� */
extern uint16_t motor_speed;
extern uint16_t knife_delay;
extern uint16_t rotate_length;
extern uint16_t delay_after;
extern uint16_t cut_count;
extern uint16_t cut_length;
extern uint16_t cut_min;

void __flash_unclock_mem(){
	FLASH->KEYR = 0x45670123;
	FLASH->KEYR = 0xCDEF89AB;
}

void __flash_lock_mem() {
  FLASH->CR |= FLASH_CR_LOCK;
}

// ������� ��������
void __flash_internal_erase() {
	while (FLASH->SR & FLASH_SR_BSY);
	if (FLASH->SR & FLASH_SR_EOP) {
		FLASH->SR = FLASH_SR_EOP;
	}

	FLASH->CR |= FLASH_CR_PER;
	FLASH->AR = ADDR_FOR_WRITE;
	FLASH->CR |= FLASH_CR_STRT;
	while (!(FLASH->SR & FLASH_SR_EOP));
	FLASH->SR = FLASH_SR_EOP;
	FLASH->CR &= ~FLASH_CR_PER;
}

//data - ��������� �� ������������ ������
//address - ����� �� flash
//count - ���������� ������������ ����, ������ ���� ������ 2
void __flash_internal_write(unsigned char* data, unsigned int address, unsigned int count) {
	__flash_unclock_mem();
	__flash_internal_erase();
	unsigned int i;

	while (FLASH->SR & FLASH_SR_BSY);
	if (FLASH->SR & FLASH_SR_EOP) {
		FLASH->SR = FLASH_SR_EOP;
	}

	FLASH->CR |= FLASH_CR_PG;

	for (i = 0; i < count; i += 2) {
		*(volatile unsigned short*)(address + i) = (((unsigned short)data[i + 1]) << 8) + data[i];
		while (!(FLASH->SR & FLASH_SR_EOP));
		FLASH->SR = FLASH_SR_EOP;
	}

	FLASH->CR &= ~(FLASH_CR_PG);
	__flash_lock_mem();
}

void flash_write_all_param(){
	// ������ � uint32
	uint32_t arr_for_write[] = {motor_speed, knife_delay, rotate_length, delay_after, cut_length};

	// ����� ��� uint8 ������ �������������� ������
	__flash_internal_write((unsigned char*) arr_for_write, (unsigned int) ADDR_FOR_WRITE, sizeof(arr_for_write));
}

uint32_t __flash_read_from_addr(uint32_t addr){
	return *(volatile uint32_t *)(addr);
}

void flash_read_all_param(){
	motor_speed 	=	__flash_read_from_addr(ADDR_FOR_WRITE);
	knife_delay 	=	__flash_read_from_addr(ADDR_FOR_WRITE + 4);
	rotate_length 	=	__flash_read_from_addr(ADDR_FOR_WRITE + 8);
	delay_after 	=	__flash_read_from_addr(ADDR_FOR_WRITE + 12);
	cut_length 		=	__flash_read_from_addr(ADDR_FOR_WRITE + 16);
}

