#include "main.h"

void init_rcc();
void wait_ms(uint32_t ms);
void initialisation();
void init_systick();
void init_start_button();
void init_cut_port();

// ����������� ������� ����� ������ ��� ���� ����� ���������� ��������� �� len_mm
uint32_t calc_length(uint32_t len_mm);


volatile uint8_t mode_start = 0;

// �������� ������
volatile uint16_t motor_speed = 2500;
// �������� ��������� ����
volatile uint16_t knife_delay = 1300;

// ������ ���������� ������� ������ ����
volatile uint16_t rotate_length = 344;  // ������ ����������

// �������� ����� ������� ������ � ��������� ����
volatile uint16_t delay_after = 250;

// ����� ���������� �����
volatile uint16_t cut_count = 0;

// ������� ����� ��������
// ���� 0 �� ����� ���������� ��� ���� �� ��������� �������
volatile uint32_t cut_length = 180;

// ������ �������� ���� � -����� / ������-
// �������� ����������� � �������� ������
volatile uint32_t cut_min = 0;

// ��� ������ ������� ��������� � ����
volatile uint32_t encoder_btn_time_start = 0;
// ��� ������ ������ ����� ����� ����������
volatile uint32_t start_btn_time_start = 0;

// ������� ����� ���������� systick
volatile uint32_t timestamp = 0;

// ���������� ��� ������ ����
volatile uint8_t menu_pos = 0;

//���������� ��� �������� ������� ������ ��� ��������� ����� �����
volatile uint32_t cut_min_time = 0;

volatile uint8_t is_reset_cut_cnt = 0;

extern uint32_t timestamp_lcd;
extern uint16_t servo_rotate_speed_hz;
extern uint8_t cursor_y;
extern uint8_t cursor_x;



int main(void)
{
//	__wait_ms(200);
	flash_read_all_param();
	initialisation();
    while(1)
    {
//    	LCD_time_test();
    	// �������� �� �������� ���� ������ ��������
    	if (mode_start){
    		//��� �������� ����� ����� � ������
    		uint32_t start_time_cut_min = cut_min_time;
    		//*********************************

    		SERVO_set_speed(motor_speed);
    		servo_rotate_forward(calc_length(cut_length));
    		servo_wait();
    		// ��� �� ����� ��� ��������
    		if (sensor_state())
    			 CUT_DOWN;
    		wait_ms(knife_delay);
    		cut_count += 1;
    		CUT_UP;
    		wait_ms(delay_after);
    		change_menu();

    		//��� �������� ����� ����� � ������
    		cut_min = 60000 / (cut_min_time - start_time_cut_min);
    		cut_min_time = 0;
    		//*********************************
    	}
    }
}

uint32_t calc_length(uint32_t len_mm){
	// �������� �� 10 �� ������� ���� ��� ������� ���� � 0.1 �� ��������
	double tmp = (double)rotate_length / (len_mm);  // ����� �������� ��� ��������� ������ ������
	double tmp_cnt = (double)ENCODER_CNT_IMPULS / tmp;
	return (uint32_t)tmp_cnt + ERROR_CORRECT;
}

// ��� ������������
void initialisation(){
	init_rcc();
	init_systick();
	LCD_init();
	change_menu();
	servo_init();
	Encoder_init_interrupt();
	init_cut_port();
	init_start_button();
	sensor_init();
//	init_timer_for_disp();
//	sensor_init();
}

void init_cut_port(){
	CUT_PORT->MODER |= 1 << (CUT_PIN << 1);
	CUT_PORT->MODER &= ~(0b10 << (CUT_PIN << 1));
}

// ��������� ������ ���������
void init_start_button(){
	// ��������� ���������� �� ������
//	EXTI->RTSR |= 1 << START_BTN_PIN;
	EXTI->FTSR |= 1 << START_BTN_PIN;
	EXTI->IMR |= 1 << START_BTN_PIN;
	SYSCFG->EXTICR[0] |= START_BTN_SYSCFG;  // �������� ���� �� ������� ����� ����������
	NVIC_EnableIRQ(START_BTN_IRQ);
	NVIC_SetPriority(START_BTN_IRQ, START_BTN_PRIORITY);
}

// ��������� �� ������ ������
void START_BTN_IRQHandler{
	if (is_reset_cut_cnt){
		is_reset_cut_cnt = 0;
	} else {
		if (mode_start)
			mode_start = 0;
		else
			mode_start = 1;
	}
	EXTI->PR |= 1 << START_BTN_PIN;
	change_menu();
}

// ��������� ������������
void init_rcc(){

	// ��������� flash
	FLASH->ACR |= FLASH_ACR_LATENCY | FLASH_ACR_PRFTBE;

	// ��������������� MCO - ����� ��� ���������� �� ��������
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	GPIOA->MODER |= GPIO_MODER_MODER8_1;  // alternate function
	GPIOA->AFR[1] &= ~GPIO_AFRH_AFRH0;
//	GPIOA->AFR[1] |= 0b0101 << 4;
	RCC->CFGR &= ~RCC_CFGR_MCO;
	RCC->CFGR |= RCC_CFGR_MCO_PLL;
	// �������� MCO ���������

	// �������� ���� �� HSE
//	RCC->CR |= RCC_CR_HSEON;
	// ���� ���������� HSE
//	while(!(RCC->CR & RCC_CR_HSERDY)){}

	// ����� ���������� PLL ������ system clock �� HSI
	RCC->CFGR &= ~RCC_CFGR_SW;
	while(!(RCC->CR & RCC_CR_HSIRDY)){}

	// ��������� RCC
	RCC->CR &= ~RCC_CR_PLLON;
	// ��������� �������� PLL - ������ ����� pll �������
	RCC->CFGR |= RCC_CFGR_PLLMUL8;
//	RCC->CFGR |= RCC_CFGR_PLLMULL3;
	// ��������� source(PLLSRC) ��������� ��� pll ������������ ����� pll ��������
	RCC->CFGR &= ~(1 << 16);
	// �������� RCC
	RCC->CR |= RCC_CR_PLLON;

	// System clock switch
	RCC->CFGR &= ~RCC_CFGR_SW;
	RCC->CFGR |= RCC_CFGR_SW_PLL;

	/*
	 * �� ������ ����� ������� �� sysclock = 32MHz
	 * PLLMULL ����� �� ��������� �� 8
	 * ���������� ��� PLL �������� HSI crystal 8MHz
	 */

	/*
	 * �������� ������������ APB1 � APB2
	 */
	RCC->CFGR &= ~RCC_CFGR_PPRE;
	RCC->CFGR |= RCC_CFGR_PPRE_DIV8;

	/*
	 * ��������� AHB prescaler
	 */
	//	RCC->CFGR |= RCC_CFGR_HPRE_DIV512;

//	RCC->AHBENR |= RCC_AHBENR_GPIODEN;
//	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
//	RCC->AHBENR |= RCC_AHBENR_GPIOEEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
//	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;

	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
//	RCC->APB1ENR |= RCC_APB1ENR_TIM7EN;
//	RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;

//	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;
}

void init_systick(){
	/* SysTick �������� ���� ������ 1 �� */
	NVIC_SetPriority(SysTick_IRQn, SYSTICK_PRIORITY);
	SysTick->CTRL= SysTick_CTRL_CLKSOURCE_Msk |
	                SysTick_CTRL_TICKINT_Msk   |
	                SysTick_CTRL_ENABLE_Msk;

	SysTick_Config(32000000 / 1000);
}

void wait_ms(uint32_t milliseconds) {
	uint32_t start = timestamp;
	while((timestamp - start) < milliseconds);
}

void SysTick_Handler(void)
{
	   //���� �������� ������ 1 ������������
	   timestamp++;
	   timestamp_lcd++;
	   cut_min_time++;

	   // ���� ������ �������� ������ � �� �����������
	   if (!(ENCODER_BUTTON_PORT->IDR & (1 << ENCODER_BUTTON_PIN))){
		   if (!Encoder_isrotated()){
			   encoder_btn_time_start += 1;
			   if (encoder_btn_time_start >= CHANGE_MENU_DELAY){
				   menu_pos += 1;
				   if (menu_pos > 2){
					   menu_pos = 0;
					   flash_write_all_param();
					   LCD_cursor_off();
				   } else if (menu_pos == 1){
					   cursor_y = 1;
					   cursor_x = 13;
					   LCD_cursor_on();
				   } else if (menu_pos == 2){
					   cursor_y = 0;
					   LCD_cursor_on();
				   }
				   encoder_btn_time_start = 0;
				   change_menu();
			   }
		   }
	   } else if (encoder_btn_time_start > 0){
		   encoder_btn_time_start = 0;
	   }

	   // ����� ����� ���������� ������� ��������� ������
	   if ((START_BTN_PORT->IDR & (1 << START_BTN_PIN)) && mode_start == 0){
		   start_btn_time_start += 1;
		   if (start_btn_time_start >= RESET_CUT_CNT_DELAY){
			   cut_count = 0;
			   start_btn_time_start = 0;
			   is_reset_cut_cnt = 1;
			   change_menu();
		   }
	   } else if (start_btn_time_start > 0){
		   start_btn_time_start = 0;
	   }
}
