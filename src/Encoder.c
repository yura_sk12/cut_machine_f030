#include "Encoder.h"

// ����� ��� ������ �� �������
#ifdef USE_EN
char * menu_text_arr[8] = {
		"Cut off ",
		"Length",
		"Cut/min ",
		"Status ",
		"Motor speed ",
		"Knife speed ",
		"Diameter ",
		"Delay after ",
};
char *status_str[2] = {"work", "stop"};
#else
char * menu_text_arr[8] = {
		"�������� ",
		"������",
		"��/��� ",
		"������-",
		"�������� ",
		"����� ���� ",
		"����� ���. ",
		"����� ����� ",
};
char *status_str[2] = {"��������", "�����"};
#endif

// ������� ������� X
volatile uint8_t cursor_x = 12;
// ������� ������� Y
volatile int8_t cursor_y = 0;

extern uint16_t motor_speed;
extern uint16_t knife_delay;
extern uint16_t rotate_length;
extern uint16_t delay_after;
extern uint16_t cut_count;
extern uint16_t cut_length;
extern uint16_t cut_min;
extern uint16_t menu_pos;
extern uint8_t mode_start;
extern uint64_t servo_rotate_steps;

uint16_t tmpcnt = 0;


// 1-���� ������� �������� ��� ������ ������
// ������������ ����� ������ ������ ��������
volatile uint8_t encoder_is_rotated = 0;

void Encoder_init_interrupt(){
	//*��������� ������*****************************
	ENCODER_PORT->MODER &= ~((0b11 << (ENCODER_PINA << 1)) | (0b11 << (ENCODER_PINB << 1)));  // input
	ENCODER_PORT->PUPDR |= (1 << (ENCODER_PINA << 1)) | (1 << (ENCODER_PINB << 1)); // pullup

	// ���� �� ������
	ENCODER_BUTTON_PORT->MODER &= ~(0b00 << (ENCODER_BUTTON_PIN << 1));  // �� ����

	// ��������� ���������� �� ������ ��������
	EXTI->RTSR |= 1 << ENCODER_BUTTON_PIN;
	EXTI->FTSR |= 1 << ENCODER_BUTTON_PIN;
	EXTI->IMR |= 1 << ENCODER_BUTTON_PIN;
	SYSCFG->EXTICR[3] |= ENCODER_SYSCFG;  // �������� ���� �� ������� ����� ����������
	NVIC_EnableIRQ(ENCODER_IRQ);
	NVIC_SetPriority(ENCODER_IRQ, ENCODER_PRIORITY);
	// ��������� ���������� �� ������ ��������

	// ��������� ���������� �� ����� ��������
//	EXTI->RTSR |= 1 << ENCODER_PINA;
	EXTI->FTSR |= 1 << ENCODER_PINA;  // ������ ����� ����� ������
	EXTI->IMR |= 1 << ENCODER_PINA;
	SYSCFG->EXTICR[2] |= ENCODER_PINA_SYSCFG;  // �������� ���� �� ������� ����� ����������
	NVIC_EnableIRQ(ENCODER_IRQ);
	NVIC_SetPriority(ENCODER_IRQ, ENCODER_PRIORITY);
	// ��������� ���������� �� ����� ��������
	//**********************************************
}

void set_cursor_pos(){
	LCD_set_position(cursor_x, cursor_y);
}

uint8_t Encoder_isrotated(){
	return encoder_is_rotated;
}

void set_cursor_y(uint8_t pos){
	cursor_y = pos%4;
}

void set_cursor_x(uint8_t pos){
	cursor_x = 12 + pos%4;
}

// ���������� 1 10 100 1000
uint16_t __get_dozens(uint8_t x){
	switch (x) {
		case 15:
			return 1;
			break;
		case 14:
			return 10;
			break;
		case 13:
			return 100;
			break;
		case 12:
			return 1000;
			break;
		default: return 1;
			break;
	}
	return 1;
}

void ENCODER_IRQHandler(){
	// ���� ���������� �� �������� ��������
	if (EXTI->PR & (1 << ENCODER_PINA)){
		// ���� ������ ������ ��������
		uint32_t encoder_dir = !(ENCODER_PORT->IDR & 1 << ENCODER_PINB);
		if (!(ENCODER_BUTTON_PORT->IDR & (1 << ENCODER_BUTTON_PIN))){
			if (menu_pos == 2){  // ���� �������� ���� �� ���������� �������
				encoder_is_rotated = 1;
				if (encoder_dir){
					// ���������
					cursor_y += 1;
					if (cursor_y > 3)
						cursor_y = 0;
					if (cursor_y >= 2 && menu_pos == 2 && cursor_x < 13){
						cursor_x = 13;
					}
				} else {
					// ���������
					cursor_y -= 1;
					if (cursor_y < 0)
						cursor_y = 3;
					if (cursor_y >= 2 && menu_pos == 2 && cursor_x < 13){
						cursor_x = 13;
					}
				}
			}
		} else { // ������ �������� �� ������
			if (encoder_dir){
				// ���������
				if (menu_pos == 2){
					switch (cursor_y) {
						case 0:  {
							// motor_speed
							if (motor_speed < 25000)
								motor_speed += __get_dozens(cursor_x);
							if (motor_speed > 25000)
								motor_speed -= __get_dozens(cursor_x);
							break;
						}
						case 1:  {
							// knife_speed
							if (knife_delay < 9999)
								knife_delay += __get_dozens(cursor_x);
							if (knife_delay > 9999)
								knife_delay -= __get_dozens(cursor_x);
							break;
						}
						case 2:  {
							// rotate_length
							if (rotate_length < 999)
								rotate_length += __get_dozens(cursor_x);
							if (rotate_length > 999)
								rotate_length -= __get_dozens(cursor_x);
							break;
						}
						case 3:  {
							// delay_after
							if (delay_after < 999)
								delay_after += __get_dozens(cursor_x);
							if (delay_after > 999)
								delay_after -= __get_dozens(cursor_x);
							break;
						}
						default:
							break;
					}
				} else if (menu_pos == 1){
					if (cut_length < 9999)
						cut_length += __get_dozens(cursor_x);
					if (cut_length > 9999)
						cut_length -= __get_dozens(cursor_x);
				} else {
					servo_rotate_forward(250);
				}
			} else {
				// ���������
				if (menu_pos == 2){
					switch (cursor_y) {
						case 0:  {
							// motor_speed
							motor_speed -= __get_dozens(cursor_x);
							if (motor_speed > 25000)
								motor_speed += __get_dozens(cursor_x);
							break;
						}
						case 1:  {
							// knife_speed
							knife_delay -= __get_dozens(cursor_x);
							if (knife_delay > 9999)
								knife_delay += __get_dozens(cursor_x);
							break;
						}
						case 2:  {
							// rotate_length
							rotate_length -= __get_dozens(cursor_x);
							if (rotate_length > 999)
								rotate_length += __get_dozens(cursor_x);
							break;
						}
						case 3:  {
							// delay_after
							delay_after -= __get_dozens(cursor_x);
							if (delay_after > 999)
								delay_after += __get_dozens(cursor_x);
							break;
						}
						default:
							break;
					}
				} else if (menu_pos == 1){
					cut_length -= __get_dozens(cursor_x);
					if (cut_length > 9999)
						cut_length += __get_dozens(cursor_x);
				} else {
					servo_rotate_backward(250);
				}
			}
		}
		change_menu();
		// ���������� ���� ����������
		EXTI->PR |= 1 << ENCODER_PINA;
	}
	// ���� ���������� �� ������ ��������
	if (EXTI->PR & (1 << ENCODER_BUTTON_PIN)){
		// ��������� ��������� �� �� ������ ��������
			if (EXTI->PR & 1 << ENCODER_BUTTON_PIN){
				// ������� �������� - ����� ����
				// ������ ���� ������
				if (ENCODER_BUTTON_PORT->IDR & (1 << ENCODER_BUTTON_PIN)){
					if (menu_pos == 2 || menu_pos == 1){
						// ���� ������� �������� ��� ������� ������
						if (!encoder_is_rotated)
							cursor_x += 1;
						if (cursor_x > 15){  // ������������ ����������� �� �
							if (cursor_y >= 2 && menu_pos == 2){
								cursor_x = 13;
							} else if (cursor_y == 1 && menu_pos == 1){
								cursor_x = 12;
							} else{
								cursor_x = 12;
							}
						}
					}
				} else {
					// ������ ���� ������

				}
				encoder_is_rotated = 0;
				// ���������� ���� ����������
				EXTI->PR |= 1 << ENCODER_BUTTON_PIN;
			}

			set_cursor_pos();
	}

}

void change_menu(){
	LCD_clear();
	if (menu_pos == 0 || menu_pos == 1){
		LCD_set_position(0,0);
		LCD_send_string((uint8_t *)menu_text_arr[0]);  // ������� ��������
//		LCD_set_position(15, 0);
		LCD_send_int(cut_count, 15, 0);

		LCD_set_position(0,1);
		LCD_send_string((uint8_t *)menu_text_arr[1]);  // ����� ��������
//		LCD_set_position(15, 1);
		LCD_send_int(cut_length, 15, 1);

		LCD_set_position(0,2);
		LCD_send_string((uint8_t *)menu_text_arr[2]);  // ������� ����� �� � ���.
//		LCD_set_position(15, 2);
		LCD_send_int(cut_min, 15, 2);

		LCD_set_position(0,3);
		LCD_send_string((uint8_t *)menu_text_arr[3]);  // ��������� ������
//		LCD_set_position(15, 3);
		if (mode_start)
			LCD_send_string((uint8_t *)status_str[0]);
		else
			LCD_send_string((uint8_t *)status_str[1]);
	} else if (menu_pos == 2){
		LCD_set_position(0,0);
		LCD_send_string((uint8_t *)menu_text_arr[4]);  // ������� ������
//		LCD_set_position(15, 0);
		LCD_send_int(motor_speed, 15, 0);

		LCD_set_position(0,1);
		LCD_send_string((uint8_t *)menu_text_arr[5]);  // ����� ��������� ����
//		LCD_set_position(15, 1);
		LCD_send_int(knife_delay, 15, 1);

		LCD_set_position(0,2);
		LCD_send_string((uint8_t *)menu_text_arr[6]);  // ������� ����
//		LCD_set_position(15, 2);
		LCD_send_int(rotate_length, 15, 2);

		LCD_set_position(0,3);
		LCD_send_string((uint8_t *)menu_text_arr[7]); // ������� ����� ���������
//		LCD_set_position(15, 3);
		LCD_send_int(delay_after, 15, 3);
	}
	set_cursor_pos();
}

